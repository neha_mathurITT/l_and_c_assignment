#This function takes the input from user that is the value of the blog name.
def blog_name_by_user():
    blog_name_message = "Enter the tumblr blog name:"
    blog_name = input(blog_name_message + "\n")
    return blog_name

#This function takes the input from user that is the value of the range of posts.
def post_range_by_user():
    post_range_message = "Enter the range:"
    post_range = input(post_range_message + "\n")
    return post_range

#This function will validate the blog name entered by the user.
def is_blog_name_valid(blog_name):
    for character in blog_name:
        if ('A' >= character <= 'Z') or ('a' >= character <= 'z'):
            return_value = True
        else:
            return_value = False
            break
    return return_value

#This function will validate the range entered by user.
def is_post_range_valid(range_entered):
    min_range = range_entered[0]
    max_range = range_entered[1]
    return min_range.isdigit() and max_range.isdigit()
