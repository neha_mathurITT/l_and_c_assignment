import json
import requests
from blogname_and_range_validation import blog_name_by_user, post_range_by_user, is_blog_name_valid, is_post_range_valid

#This function will print the image urls from the given post range.
def display_image_urls(json_data, parameters, no_of_posts):
    post_count = int(parameters['start'])
    image_quality = "photo-url-1280"
    #This will fetch more than 50 images as per entered by user
    while post_count != no_of_posts:
        if post_count > int(parameters['num']) + 1:
            break
        for post in json_data['posts']:
            post_count += 1
            url_of_image = ""
            if post_count > int(parameters['num']) + 1:
                break
            if len(post['photos']) > 0:
                for photo in post['photos']:
                    url_of_image += photo[image_quality] + "\n   "
                url_of_image = url_of_image[:-4]
            else:
                url_of_image = post[image_quality]
            print(str(post_count) + ". " + url_of_image)

#This function will print inforamtion like title,description,name and number of posts.
def display_basic_info_of_posts(json_data, parameters):
    title = "Title: "
    name = "Name: "
    description = "Description: "
    no_of_posts = "No of Posts: "
    print(title + json_data['tumblelog']['title'])
    print(name + json_data['tumblelog']['name'])
    print(description + json_data['tumblelog']['description'])
    print(no_of_posts + str(json_data['posts-total']) + "\n")
    if json_data['posts-total'] == 0:
        print("No posts are found for this blog name!")
    else:
        display_image_urls(json_data, parameters, json_data['posts-total'])

def get_response_from_tumblr_api(url, parameters):
    response_object = requests.get(url=url, params=parameters)
    response_content = response_object.text
    length_of_response_content = len(response_content)
    # valid json from the response_content is taken out
    json_from_response = response_content[22:length_of_response_content-2]
    if "<title>Not found.</title>" in str(json_from_response):
        print("Blog does not exists!")
    else:
        # Decoding the json using json.loads() method
        json_data = json.loads(json_from_response)
        display_basic_info_of_posts(json_data, parameters)

def url_for_tumblr_api(blog_name, post_range):
    # The post offset to start from.
    start_post_number = str(int(post_range[0]) - 1)
    # The end number of post to return
    end_post_number = str(int(post_range[1]) - 1)
    posts_range = "&num=" + end_post_number + "&start=" + start_post_number
    tumblr_api_url = "https://"+ blog_name + ".tumblr.com/api/read/json?type=photo" + posts_range
    url = tumblr_api_url
    parameters = {'name':blog_name, 'start':start_post_number, 'num':end_post_number}
    get_response_from_tumblr_api(url, parameters)

def main():
    blog_name_entered = False
    post_range_entered = False
    while not blog_name_entered:
        blog_name = blog_name_by_user()
        is_blog_name_valid(blog_name)
        if len(blog_name) != 0:
            blog_name_entered = True
            while not post_range_entered:
                post_range = post_range_by_user()
                if "-" in post_range:
                    range_entered = post_range.split("-")
                    post_range_result = is_post_range_valid(range_entered)
                    if post_range_result:
                        post_range_entered = True
                        url_for_tumblr_api(blog_name, range_entered)
                else:
                    print("Please enter the range of posts in the specified format: Start_post_number - End_post_number")
                    post_range_entered = False
        else:
            print("Please enter a valid blog name")
            blog_name_entered = False

main()
