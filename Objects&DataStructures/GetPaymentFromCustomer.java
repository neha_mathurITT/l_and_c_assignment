class Customer
{ 
     private String firstName; 
     private String lastName;    
     private Wallet myWallet = new Wallet();
     public String getFirstName(){ return firstName;}
     public String getLastName(){ return lastName; }
     public void getPayment(float bill){
          String paidSuccessfullyMessage = "Thank you for paying!";
          String takeMoneyLaterMessage = "Please come back later and get my money!";
          try{			
               if (myWallet.getTotalMoney() >= bill){
                    myWallet.subtractMoney(bill);
                    System.out.println(paidSuccessfullyMessage);
                    }
               else{
                    throw new InSufficientAmountInWalletException(takeMoneyLaterMessage);
                    }
          }
          catch(InSufficientAmountInWalletException exception){
               System.out.println(exception.getExceptionMessage());
               }
     }
}

class InSufficientAmountInWalletException extends Exception{
     String exceptionMessage;
     public InSufficientAmountInWalletException(String message){
          exceptionMessage = message;
     }
     public String getExceptionMessage(){
          return exceptionMessage;
     }
}

class Wallet
{
     public float balance = 10; 
     public float getTotalMoney() { return balance; }
     public void setTotalMoney(float newBalance) {
          balance = newBalance; } 
     public void addMoney(float deposit) {
          balance += deposit; } 
     public float subtractMoney(float debit) {
          balance -= debit;
          return balance; } 
}
	
public class GetPaymentFromCustomer
{
     public static void main(String args[])
     {
          float payment = 2;
          Customer customer = new Customer();
          customer.getPayment(payment);  
     }
}
