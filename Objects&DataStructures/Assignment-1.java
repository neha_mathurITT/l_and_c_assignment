Class Employee { 
  string name;
  int age; 
  float salary; 
  public : string getName(); 
  void setName(string name); 
  int getAge(); 
  void setAge(int age); 
  float getSalary();
  void setSalary(float salary);
}; 
Employee employee; 

Is 'employee' an object or a data structure?

=> The class Employee is a data structure as it contains only the data and no meaningful function(behaviour). Also, according to the defination of DTO(Data Transfer Object) which contains 
      only variables and no functions. So, employee is an data structure which only exposes data and has no meaningful function in it.
      If the class contains some behaviour then it would be an object.