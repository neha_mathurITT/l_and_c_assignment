#Python user-defined exceptions
class Error(Exception):
    pass

class AtmException(Error):
    pass

class AccountException(Error):
    pass

class CardException(Error):
    pass

class InvalidCardException(CardException):
    pass

class CardBlockedException(CardException):
    pass

class InSufficientAmountInATMException(AtmException):
    pass

class InvalidPinException(AtmException):
    pass

class InSufficientAmountInUserAccountException(AccountException):
    pass
