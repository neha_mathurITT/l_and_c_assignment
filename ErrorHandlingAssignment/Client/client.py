import socket
from stringLiterals import connectionFailedMessage

def check_connection_with_server():
    #Create a socket object
    socketObj = socket.socket()
    portNumber = 12345
    try:
        socketObj.connect(('127.0.0.1', portNumber))
        # receive data from the server
        # data = socketObj.recv(1024)
        # print(data.decode())
        socketObj.close()
        return True
    except ConnectionRefusedError:
        print(connectionFailedMessage)
