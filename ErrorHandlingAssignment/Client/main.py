from receipt import print_receipt
from userInformation import userInformation
from stringLiterals import *
from client import check_connection_with_server
from userDefinedExceptions import InSufficientAmountInATMException, InvalidCardException, CardBlockedException, InSufficientAmountInUserAccountException, InvalidPinException

amountInATMMachine = 10000
amountInUserAccount = userInformation.get("balance")

def withdraw_cash(amount):
    try:
        if(amount <= amountInATMMachine and amount <= amountInUserAccount):
            amountleftInUserAccount = amountInUserAccount - amount
            return amountleftInUserAccount
        elif amount > amountInATMMachine:
            raise InSufficientAmountInATMException(insufficientAmountInATMMessage)
        elif amount > amountInUserAccount:
            raise InSufficientAmountInUserAccountException(insufficientAmountInUserAccountMessage)

    except InSufficientAmountInATMException:
        print(insufficientAmountInATMMessage)
        main()

    except InSufficientAmountInUserAccountException:
        print(insufficientAmountInUserAccountMessage)
        main()

def receipt_choices(amount, amountRemainingInUserAccount, accountOpted):
    receipt = int(input(receiptMessage))
    if((receipt == 1 and accountOpted == 2) or (receipt == 2 and accountOpted == 2)):
        print(transactionDeniedMessage)
        main()
    elif receipt == 1 and accountOpted == 1:
        print_receipt(amount, amountRemainingInUserAccount)
    else:
        print(transactionSuccessMessage)
    main()

def enter_amount_for_withdrawal(accountOpted):
    amount = int(input(enterAmountMsg))
    amountRemainingInUserAccount = withdraw_cash(amount)
    receipt_choices(amount, amountRemainingInUserAccount, accountOpted)

def account_choices():
    try:
        accountOpted = int(input(accountType))
    except ValueError:
        print(invalidNumber)
        account_choices()
    if accountOpted in (1,2):
        enter_amount_for_withdrawal(accountOpted)
    else:
        print(accountOptMessage)
        account_choices()

def transaction_choices():
    try:
        userChoice = int(input(transactions))
    except ValueError:
        print(invalidNumber)
        transaction_choices()

    if userChoice == 4:
        account_choices()
    elif userChoice == 1:
        print("--------")
        print("Current balance: ", amountInUserAccount)
        main()
    else:
        print(cashWithdrawalMessage)
        transaction_choices()

def get_pin_from_database():
    return userInformation.get("pin")

def get_pin_from_user(pinCounter):
    try:
        if pinCounter == 4:
            raise CardBlockedException
    except CardBlockedException:
        print(cardBlockedMessage)
        main()
    pinEnteredByUser = ""
    if pinCounter <= 3:
        try:
            pinEnteredByUser = int(input(enterPinMessage))
        except ValueError:
            print(invalidPinMessage)
    return pinEnteredByUser

def card_swipe_count(count):
    return count+1

def validate_pin_for_transactions():
    pinStoredInDatabase = get_pin_from_database()
    pinCount = 1
    pinEnteredByUser = get_pin_from_user(pinCount)
    while pinCount < 4:
        try:
            if pinStoredInDatabase == pinEnteredByUser:
                transaction_choices()
            else:
                pinCount = card_swipe_count(pinCount)
                raise InvalidPinException(invalidPinMessage)
        except InvalidPinException:
            if pinCount <= 3:
                print(invalidPinMessage)
            pinEnteredByUser = get_pin_from_user(pinCount)

def get_language_opted_by_user():
    try:
        languageChoosen = int(input(languageOptions))
    except ValueError:
        print(invalidNumber)
        get_language_opted_by_user()

    if languageChoosen == 1:
        validate_pin_for_transactions()
    elif languageChoosen in (2, 3):
        print(languageOptMessage)
        get_language_opted_by_user()
    else:
        print(invalidChoiceSelectedMessage)
        get_language_opted_by_user()

def get_card_number_from_database():
    return userInformation.get('cardNumber')

def card_swipe_process():
    cardNumber = int(input(enterCardNumberMessage))
    if check_connection_with_server():
        cardNumberStoredInDatabase = get_card_number_from_database()
        try:
            if cardNumber == cardNumberStoredInDatabase:
                get_language_opted_by_user()
            else:
                raise InvalidCardException
        except InvalidCardException:
            print(invalidCardMessage)
            card_swipe_process()
    else:
        main()

def main():
    print(welcomeMessage)
    card_swipe_process()

while True:
    main()
