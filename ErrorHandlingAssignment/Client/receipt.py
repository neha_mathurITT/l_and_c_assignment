import datetime
from userInformation import userInformation

date = datetime.datetime.now()
accountNumber = userInformation.get("accountNumber")

def print_receipt(amount, amountLeftAfterTransaction):
    print("------------------------------------------\n\t\tCentral Bank")
    print()
    print("Date:", date.strftime("%d-%m-%Y"), "         ", "Time:", date.strftime("%H:%M:%S"))
    print("Card: ************" + str(accountNumber)[-4:])
    print("Withdrawal amount:", amount)
    print("Available Balance:", amountLeftAfterTransaction)
    print("------------------------------------------")
