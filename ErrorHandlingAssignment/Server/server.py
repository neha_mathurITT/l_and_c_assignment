import socket                
  
#create a socket object 
socketObj = socket.socket()          
portNumber = 12345               
socketObj.bind(('', portNumber))         

#put the socket into listening mode 
socketObj.listen(5)      
print ("Server is up for connections")            

while True:
    # Establish connection with client. 
    connection, address = socketObj.accept()      
    print ("Got connection from", address) 

    # send a thank you message to the client. 
    msg = "Thank you for connecting with server" 
    connection.send(msg.encode()) 

    # Close the connection with the client 
    connection.close() 