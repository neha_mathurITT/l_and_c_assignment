from string_literals import *
from geocode_api import *

class GeoCodeApplication:
    def city_name_input(self):
        city_name = input(cityNameMsg)
        return city_name

    def postal_code_input(self):
        postal_code = input(postalCodeMsg)
        return postal_code

    def take_input_from_user(self):
        while True:
            try:
                user_input = int(input(findLatAndLon))
                if user_input == 1:
                    city_name = self.city_name_input()
                    return city_name, user_input

                elif user_input == 2:
                    postal_code = self.postal_code_input()
                    return postal_code, user_input

                else:
                    print(invalidChoiceMsg)

            except ValueError:
                print(valueErrorMessage)

    def display_latitude_and_longitude(self, latitude, longitude, location_name):
        lat = latitude.split('.')[1]
        lon = longitude.split('.')[1]
        print(location, location_name)
        if(len(lat) > 4 or len(lon) > 4):
            print(latitudeMsg, round(float(latitude), 4))
            print(longitudeMsg, round(float(longitude), 4))
        else:
            print(latitudeMsg, float(latitude))
            print(longitudeMsg, float(longitude))

def main():
    geo_code_app = GeoCodeApplication()
    area, user_input = geo_code_app.take_input_from_user()
    geo_code_api_object = GeoCodeApi()
    if user_input == 1:
        api_url = geo_code_api_object.geo_code_api_url_for_city(area)
    elif user_input == 2:
        api_url = geo_code_api_object.geo_code_api_url_for_postalcode(area)
    response_status = geo_code_api_object.get_request_status_of_geocode_api(api_url)
    if not response_status:
        pass
    else:
        lat, lon, area_name = geo_code_api_object.get_response_from_url(response_status)
        geo_code_app.display_latitude_and_longitude(lat, lon, area_name)

main()
