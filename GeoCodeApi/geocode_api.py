import socket
import requests
from string_literals import *
from geocode_api_exceptions import *

class GeoCodeApi:
    def get_api_key(self):
        with open(".env", "r") as fp:
            key = fp.readline()
        return key

    def get_latitude_longitude(self, api_response_in_json):
        latitude = str(api_response_in_json['lat'])
        longitude = str(api_response_in_json['lon'])
        location_name = api_response_in_json['display_name']
        return latitude, longitude, location_name

    def get_response_from_url(self, response):            
        lat, lon, area_name = self.get_latitude_longitude(response)
        return lat, lon, area_name

    def get_request_status_of_geocode_api(self, url):
        try:
            response = requests.get(url)
            if response.status_code == 200:
                complete_json_response = response.json()
                json_response = complete_json_response[0] 
                return json_response

            elif response.status_code == 404:
                raise UnableToGeoCodeException(unableToGeoCodeMsg)

            elif response.status_code == 429:
                raise RateLimitedDayException(rateLimitedMsg)

            elif response.status_code == 500:
                raise ServerException(serverErrorMsg)

        except UnableToGeoCodeException:
            print(unableToGeoCodeMsg)
            return False

        except RateLimitedDayException:
            print(rateLimitedMsg)
            return False

        except ServerException:
            print(serverErrorMsg)
            return False

    def geo_code_api_url_for_city(self, city_name):
        key = self.get_api_key()
        try:
            url = initial_api_url + key + "&city=" + city_name + "&format=json"
            return url
        except socket.error:
            print(unfetchedApiMsg)

    def geo_code_api_url_for_postalcode(self, postal_code):
        key = self.get_api_key()
        try:
            url = initial_api_url + key + "&postalcode=" + str(postal_code) + "&format=json"
            return url
        except socket.error:
            print(unfetchedApiMsg)
