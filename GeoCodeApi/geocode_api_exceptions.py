class Error(Exception):
    pass

class GeoCodeApiException(Error):
    pass

class UnableToGeoCodeException(GeoCodeApiException):
    pass

class RateLimitedDayException(GeoCodeApiException):
    pass

class ServerException(GeoCodeApiException):
    pass
