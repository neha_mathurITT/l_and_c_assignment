import unittest.mock
from python_wrap_cases import wrap_case
from geocode_api import *
from lat_lon_values_for_testing import *
from string_literals import *

@wrap_case
class ApiTestMethods(unittest.TestCase):
    def setUp(self):
        geo_code_api_object = GeoCodeApi()
        key = geo_code_api_object.get_api_key()
        self.url_for_city = initial_api_url + geo_code_api_object.get_api_key() + "&city=%s&format=json"
        self.url_for_postal_code = initial_api_url + geo_code_api_object.get_api_key() + "&postalcode=%s&format=json"

    def get_response_in_json(self, request_url):
        url_response = requests.get(request_url)
        self.response = url_response.json()
        return self.response
        
    def fetch_response_from_geocoding_api_for_city(self, city_name):
        request_url = self.url_for_city % str(city_name)
        self.response = self.get_response_in_json(request_url)
        return self.response

    def fetch_response_from_geocoding_api_for_postal_code(self, postal_code):
        request_url = self.url_for_postal_code % str(postal_code)
        self.response = self.get_response_in_json(request_url)
        return self.response

    @wrap_case("jaipur")
    def test_response_for_city(self, area):
        self.response = self.fetch_response_from_geocoding_api_for_city(area)
        json_response = self.response[0]
        self.assertEqual(float(json_response['lat']), lat_for_city)
        self.assertEqual(float(json_response['lon']), lon_for_city)

    @wrap_case(302017)
    def test_response_for_postal_code(self, postal_code):
        self.response = self.fetch_response_from_geocoding_api_for_postal_code(postal_code)
        json_response = self.response[0]
        self.assertEqual(float(json_response['lat']), lat_for_postal_code)
        self.assertEqual(float(json_response['lon']), lon_for_postal_code)

    @wrap_case("jodhpur")
    def test_api_response(self, area_name):
        request_url = self.url_for_city % str(area_name)
        response = requests.get(request_url)
        self.assertEqual(200, response.status_code)

    @wrap_case(" @@ ")
    def test_api_response_for_invalid_area_name(self, area_name):
        request_url = self.url_for_city + str(area_name)
        response = requests.get(request_url)
        self.assertEqual(400, response.status_code)

    @wrap_case("jaipur")
    def test_api_response_for_invalid_url_string(self, city_name):
        request_url = self.url_for_city + str(city_name)+"city_name"
        response = requests.get(request_url)
        self.assertEqual(400, response.status_code)

    @wrap_case('udaipur')
    def test_api_response_for_invalid_api_key(self, city_name):
        request_url = initial_api_url + invalid_api_key + "&city="+ city_name +"&format=json"
        response = requests.get(request_url)
        self.assertEqual(401, response.status_code)

if __name__ == '__main__':
    unittest.main()
