#Assignment 1: The below program is to guess the correct number between 1 to 100

import random

def check_validity_of_number(guessed_num):
    if guessed_num.isdigit() and 1<= int(guessed_num) <=100:
        return True
    else:
        return False

def is_number_guessed_correctly(guessed_input, generated_integer,number_of_guesses):
    low_guessed_msg="Too low. Guess again"
    high_guessed_msg="Too High. Guess again"
    correct_guessed_msg="You guessed it in "

    if int(guessed_input) < generated_integer:
        print(low_guessed_msg)
        return False
    elif int(guessed_input) > generated_integer:
        print(high_guessed_msg)
        return False
    else:
        print(correct_guessed_msg + str(number_of_guesses+1) + " guesses!")
        return True
    return False

def comparison_of_numbers(guessed_input,generated_num,number_of_guesses):
    invalid_input_msg = "I wont count this one Please enter a number between 1 to 100"

    if not check_validity_of_number(guessed_input):
        print(invalid_input_msg)
        return False
    else:
        guessed_input = int(guessed_input)
        correct_guess=is_number_guessed_correctly(guessed_input,generated_num,number_of_guesses)
    return correct_guess

def main():
    generated_integer = random.randint(1,100)
    print(generated_integer)
    correct_guess = False
    number_of_guesses = 0
    guess_input_msg = "Guess a number between 1 and 100:"
    guessed_input = input(guess_input_msg)
    while not correct_guess:
        correct_guess = comparison_of_numbers(guessed_input, generated_integer,number_of_guesses)
        if correct_guess:
            break
        else:
            number_of_guesses+=1
            guessed_input = input(guess_input_msg)
main()

