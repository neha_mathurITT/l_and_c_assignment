import json
import requests

#This function will print the image urls from the given post range.
def display_image_urls(json_data,Parameters, no_of_posts):
    post_count = int(Parameters['start'])
    image_quality = "photo-url-1280"
    #This will fetch more than 50 images as per entered by user.
    for posts in range(no_of_posts):
        if(post_count > int(Parameters['num'])):
            break
        for post in json_data['posts']:
                post_count +=1
                url_of_image = ""
                if(post_count > int(Parameters['num'])):
                    break
                if len(post['photos']) > 0:
                    for photo in post['photos']:
                        url_of_image += photo[image_quality] + "\n   "
                    url_of_image = url_of_image[:-4]
                else:
                    url_of_image = post[image_quality]
                print(str(post_count) + ". " + url_of_image)


#This function will print the basic information from posts like title,description,name and number of posts.
def display_basic_info_of_posts(json_data,Parameters):
    title = "Title: "
    name = "Name: "
    description = "Description: "
    no_of_posts = "No of Posts: "
    print(title + json_data['tumblelog']['title'])
    print(name + json_data['tumblelog']['name'])
    print(description + json_data['tumblelog']['description'])
    print(no_of_posts + str(json_data['posts-total'])+"\n")
    if(json_data['posts-total']==0):
        print("No posts are found for this blog name!")
    else:
        display_image_urls(json_data,Parameters, json_data['posts-total'])

# This function will send the GET request to the API with the URL and PARAMETERS entered by user
def get_response_from_tumblr_api(URL,Parameters):
    response_object = requests.get(url = URL, params = Parameters) 
    response_content = response_object.text
    length_of_response_content = len(response_content)
    json_from_response = response_content[22:length_of_response_content-2]    # json from the response_content is taken out
    if("<title>Not found.</title>" in str(json_from_response)):
        print("Blog does not exists!")
    else:
        json_data = json.loads(json_from_response)                                # Decoding the json using json.loads() method    
        display_basic_info_of_posts(json_data,Parameters)


def url_for_tumblr_api(blog_name,post_range):
        start_post_number = str(int(post_range[0])-1)                                # The post offset to start from.
        end_post_number = str(int(post_range[1])-int(post_range[0])+1)               # The end number of post to return.
        post_type="photo"
        url_for_tumblr_api = "https://"+ blog_name + ".tumblr.com/api/read/json?type=" + post_type + "&num=" + end_post_number + "&start=" + start_post_number                                                            
        URL = url_for_tumblr_api
        Parameters = {'name':blog_name, 'start':start_post_number, 'num': end_post_number}  
        get_response_from_tumblr_api(URL,Parameters)

#This function takes the input from user that is the value of the blog name.
def blog_name_by_user():
    blog_name_message = "Enter the tumblr blog name:"
    blog_name = input(blog_name_message + "\n") 
    return(blog_name)
    
#This function takes the input from user that is the value of the range of posts
def post_range_by_user():
    post_range_message = "Enter the range:"
    post_range = input(post_range_message +"\n")
    return (post_range)

def is_blog_name_valid(blog_name):
    return_value = True
    for character in blog_name:
        if ((character >= 'A' and character <= 'Z') or (character >='a' and character <= 'z')):
            return_value = True
        else: 
            return_value = False
            break
    return return_value

def is_post_range_valid(range_entered):
    min_range = range_entered[0]
    max_range = range_entered[1]
    if(min_range.isdigit() and max_range.isdigit()):
        return True
    else:
        return False

def main():
    blog_name_entered = False
    post_range_entered = False
    while (not blog_name_entered):
        blog_name = blog_name_by_user()
        blog_name_result = is_blog_name_valid(blog_name)
        if(blog_name_result):
            blog_name_entered = True
            while not post_range_entered:
                post_range = post_range_by_user()
                range_entered = post_range.split("-")
                post_range_result = is_post_range_valid(range_entered)
                if(post_range_result):
                    post_range_entered = True
                    url_for_tumblr_api(blog_name,range_entered)      
                else:
                    print("Please enter the range of posts in the specified format: Start_post_number - End_post_number")
                    post_range_entered = False
        else: 
            print("Please enter a valid blog name")
            blog_name_entered = False
        
main()