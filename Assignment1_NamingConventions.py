import random
def roll_dice(max_value_of_dice):
    number_generated=random.randint(1, max_value_of_dice)
    return number_generated

def main():
    MAX_VALUE_OF_DICE=6
    valid_choice = True
    quit_roll = 'q'
    while valid_choice:
        choice=input("Ready to roll? Enter q to Quit")
        if choice.lower() != quit_roll:
            number_on_dice= roll_dice(MAX_VALUE_OF_DICE)
            print("You have rolled a", number_on_dice)
        else:
            valid_choice=False

main()